#version 150
//Author: Nikhil Verma
// Phong fragment shader
//
// Contributor:  Nikhil Verma

// INCOMING DATA
//in vec4 position;
//normal coming from the vertex shader
in vec4 normal;
in vec4 worldPosition;

#define SIZE 5
uniform int totalLights;
//material properties of the object
uniform vec4 Oa;
uniform vec4 Od;
uniform vec4 Os;

//reflective characteristics of the object
uniform float ka;
uniform float kd;
uniform float ks;
uniform float ns;

//properties of the light source
uniform vec4 Id[SIZE];
//specular light in thte scene
uniform vec4 Is[SIZE];
uniform vec4 lightPos[SIZE];//this still needs to be converted into clip space

//ambient light in the scene
uniform vec4 Ia;


// OUTGOING DATA

// final fragment color
out vec4 fragmentColor;

void main()
{
    vec3 n=normalize(normal.xyz);
    vec3 l=normalize(-worldPosition.xyz);
    vec3 r=normalize(reflect(l,n));
    vec3 v=normalize(worldPosition.xyz);
//    vec3 lc=lightPos[0];

    int i=0;
    
    fragmentColor.r=Ia.r*ka*Oa.r ;
    fragmentColor.g=Ia.g*ka*Oa.g ;
    fragmentColor.b=Ia.b*ka*Oa.b ;
    fragmentColor.a=Ia.a*ka*Oa.a ;
    for(i=0;i<totalLights;i++){
        fragmentColor.r+= Id[i].r*kd*Od.r*dot(l,n) + Id[i].r*ks*Os.r*pow(max(0.0f,dot(r,v)),ns);
        fragmentColor.g+= Id[i].g*kd*Od.g*dot(l,n) + Id[i].g*ks*Os.g*pow(max(0.0f,dot(r,v)),ns);
        fragmentColor.b+= Id[i].b*kd*Od.b*dot(l,n) + Id[i].b*ks*Os.b*pow(max(0.0f,dot(r,v)),ns);
        fragmentColor.a+= Id[i].a*kd*Od.a*dot(l,n) + Id[i].a*ks*Os.a*pow(max(0.0f,dot(r,v)),ns);
    }

}
