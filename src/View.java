//Author: Nikhil Verma
import Jama.Matrix;

import javax.media.opengl.GL3;

/**
 * Created by NikhilVerma on 18/05/16.
 */
public class View {
    // current values for transformations
    private float rotateDefault[];
    private float translateDefault[];
    private float scaleDefault[];

    // current view values
    float eyePoint[];
    float lookAt[];
    float up[];

    // clipping window boundaries
    private float left = -1.0f;
    private float right = 1.0f;
    private float top = 1.0f;
    private float bottom = -1.0f;
    private float near =3.0f;
    private float far = 100.5f;

    /**
     * constructor
     */
    public View()
    {
        rotateDefault = new float[3];
        rotateDefault[0] = 0.0f;
        rotateDefault[1] = 50.0f;
        rotateDefault[2] = 90.0f;

        translateDefault = new float[3];
        translateDefault[0] = 1.0f;
        translateDefault[1] = 0.0f;
        translateDefault[2] = -1.0f;

        scaleDefault = new float[3];
        scaleDefault[0] = 1.0f;
        scaleDefault[1] = 4.0f;
        scaleDefault[2] = 1.0f;

        eyePoint = new float[3];
        eyePoint[0] = 0.0f;
        eyePoint[1] = 3.0f;
        eyePoint[2] = 3.0f;

        eyePoint[0] = 0.0f;
        eyePoint[1] = 0.0f;
        eyePoint[2] = 0.0f;

        lookAt = new float[3];
        lookAt[0] = 1.0f;
        lookAt[1] = 0.0f;
        lookAt[2] = 0.0f;

        lookAt[0] = 0.0f;
        lookAt[1] = 0.0f;
        lookAt[2] = -1.0f;

        up = new float[3];
        up[0] = 0.0f;
        up[1] = 1.0f;
        up[2] = 0.0f;
    }

    /**
     * This function sets up the camera parameters controlling the viewing
     * transformation.
     * @param eyepointX - x coordinate of the camera location
     * @param eyepointY - y coordinate of the camera location
     * @param eyepointZ - z coordinate of the camera location
     * @param lookatX - x coordinate of the lookat point
     * @param lookatY - y coordinate of the lookat point
     * @param lookatZ - z coordinate of the lookat point
     * @param upX - x coordinate of the up vector
     * @param upY - y coordinate of the up vector
     * @param upZ - z coordinate of the up vector
     */
    void setUpCamera(float eyepointX, float eyepointY, float eyepointZ,
                      float lookatX, float lookatY, float lookatZ,
                      float upX, float upY, float upZ ){
        eyePoint[0]=eyepointX;
        eyePoint[1]=eyepointY;
        eyePoint[2]=eyepointZ;

        lookAt[0]=lookatX;
        lookAt[1]=lookatY;
        lookAt[2]=lookatZ;

        up[0]=upX;
        up[1]=upY;
        up[2]=upZ;
    }

    public void sendProjectionViewInfo(GL3 gl3,int program){

        int leftLoc = gl3.glGetUniformLocation( program , "left" );
        int rightLoc = gl3.glGetUniformLocation( program , "right" );
        int topLoc = gl3.glGetUniformLocation( program , "top" );
        int bottomLoc = gl3.glGetUniformLocation( program , "bottom" );
        int nearLoc = gl3.glGetUniformLocation( program , "near" );
        int farLoc = gl3.glGetUniformLocation( program , "far" );

        gl3.glUniform1f( leftLoc, left );
        gl3.glUniform1f( rightLoc, right );
        gl3.glUniform1f( topLoc, top );
        gl3.glUniform1f( bottomLoc, bottom );
        gl3.glUniform1f( nearLoc, near );
        gl3.glUniform1f( farLoc, far );

        int posLoc = gl3.glGetUniformLocation( program, "cPosition" );
        int lookLoc = gl3.glGetUniformLocation( program, "cLookAt" );
        int upVecLoc = gl3.glGetUniformLocation( program, "cUp" );

        // send down to the shader
        gl3.glUniform3fv( posLoc, 1, eyePoint, 0 );
        gl3.glUniform3fv( lookLoc, 1, lookAt, 0 );
        gl3.glUniform3fv( upVecLoc, 1, up, 0 );
    }

    public Matrix getProjectionViewMatrix(){

        //projection
        Matrix projection=Matrix.identity(4, 4);
        projection.set(0,0,(2.0*near)/(right-left));
        projection.set(1,1,(2.0*near)/(top-bottom));
        projection.set(2,0,(right+left)/(right-left));
        projection.set(2,1,(top+bottom)/(top-bottom));
        projection.set(2,2,((-1.0*(far+near))/(far-near)));
        projection.set(2,3,-1.0);
        projection.set(3,2,(-2.0*far*near)/(far-near));
        projection.set(3,3,0);//mind the difference

//        projection=projection.transpose();

        //view
        float [] n=normalize(subtract(eyePoint, lookAt));
        float [] u=normalize(cross(normalize(up),n));
        float [] v=normalize(cross(n,u));

        Matrix view=Matrix.identity(4,4);
        view.set(0,0,u[0]);
        view.set(1,0,u[1]);
        view.set(2,0,u[2]);
        view.set(3,0,-1.0*(dot(u, eyePoint)));

        view.set(0,1,v[0]);
        view.set(1,1,v[1]);
        view.set(2,1,v[2]);
        view.set(3,1,-1.0*(dot(v, eyePoint)));

        view.set(0,2,n[0]);
        view.set(1,2,n[1]);
        view.set(2,2,n[2]);
        view.set(3,2,-1.0*(dot(n, eyePoint)));

//        view=view.transpose();

        return projection.times(view);
//        return view.times(projection);
//        return projection;
//        return projection;
    }

    public Matrix getCompositeMatrix(Matrix transformation){

        //projection
        Matrix projection=Matrix.identity(4,4);
        projection.set(0,0,(2.0*near)/(right-left));
        projection.set(1,1,(2.0*near)/(top-bottom));
        projection.set(2,0,(right+left)/(right-left));
        projection.set(2,1,(top+bottom)/(top-bottom));
        projection.set(2,2,((-1.0*(far+near))/(far-near)));
        projection.set(2,3,-1.0);
        projection.set(3,2,(-2.0*far*near)/(far-near));
        projection.set(3,3,0);//mind the difference

//        projection=projection.transpose();

        //view
        float [] n=normalize(subtract(eyePoint, lookAt));
        float [] u=normalize(cross(normalize(up),n));
        float [] v=normalize(cross(n,u));

        Matrix view=Matrix.identity(4,4);
        view.set(0,0,u[0]);
        view.set(1,0,u[1]);
        view.set(2,0,u[2]);
        view.set(3,0,-1.0*(dot(u, eyePoint)));

        view.set(0,1,v[0]);
        view.set(1,1,v[1]);
        view.set(2,1,v[2]);
        view.set(3,1,-1.0*(dot(v, eyePoint)));

        view.set(0,2,n[0]);
        view.set(1,2,n[1]);
        view.set(2,2,n[2]);
        view.set(3,2,-1.0*(dot(n, eyePoint)));

//        view=view.transpose();

        return projection.times(view.times(transformation));
//        return view.times(projection);
//        return projection;
//        return projection;
    }

    private static float [] normalize(float [] a){
        float mag=(float)Math.sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]);
        float [] normalized=new float[3];
        for (int i = 0; i < 3; i++) {
            normalized[i]=a[i]/mag;
        }
        return normalized;
    }

    private float [] subtract(float[] a,float [] b){
        float []difference=new float[3];
        for (int i = 0; i < 3; i++) {
            difference[i]=a[i]-b[i];
        }
        return difference;
    }

    private float [] cross(float []a,float [] b){
        float [] crossProduct=new float[3];
        crossProduct[0]=a[1]*b[2]-a[2]*b[1];
        crossProduct[1]=a[2]*b[0]-a[0]*b[2];
        crossProduct[2]=a[0]*b[1]-a[1]*b[0];
        return crossProduct;
    }

    private float dot(float [] a,float [] b){
        float dotProduct=0;
        for (int i = 0; i < 3; i++) {
            dotProduct+=a[i]*b[i];
        }
        return dotProduct;
    }

}
