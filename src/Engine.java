//Author: Nikhil Verma
import Jama.Matrix;
import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import javax.media.opengl.*;
import javax.media.opengl.awt.GLCanvas;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.util.Vector;

/**
 * Main rendering graphics engine that holds information about the
 * models,lights and view.
 * Created by NikhilVerma on 18/05/16.
 */
public class Engine implements GLEventListener{

    private final GLCanvas canvas;
    private View view;
    private Vector<LightSource> lights=new Vector<>();
    private Vector<Model> models=new Vector<>();
    float[] Ia ={0.5f,0.5f,0.5f,1.0f};
    Animator animator;
    float cameraAngle=0;

    public Engine() {
        // GL setup
        GLProfile glp = GLProfile.get( GLProfile.GL3 );
        GLCapabilities capabilities = new GLCapabilities( glp );
        canvas = new GLCanvas( capabilities);
        canvas.addGLEventListener(this);
    }

    public GLCanvas getCanvas() {
        return canvas;
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    @Override
    public void init(GLAutoDrawable drawable) {
        // get the gl object
        GL3 gl3 = drawable.getGL().getGL3();

        // create the Animator now that we have the drawable
        animator = new Animator( drawable );

        //Load up a scene
        view =new View();
        Model table= makeModel(gl3, new ObjFromProperties("table.properties"), "texturephong","wood.jpg");
        table.setUpTransforms(1,1,1,
                0,90,0,
                0,0,0);
        table.kd=1.7f;

        Model teapot = makeModel(gl3, new Teapot(), "generic");
        teapot.setUpTransforms(0.5f,0.5f,0.5f,
                0,-45,0,
                0.6f,1.1f,0.4f);
        Model book = makeModel(gl3, new Book(), "textured","book.png");
        book.setUpTransforms(0.3f,0.3f,0.3f,
                0,0,0,
                0.1f,1.1f,-0.1f);
        Model chair= makeModel(gl3, new ObjFromProperties("chair.properties"), "texturephong","wood.jpg");
        chair.setUpTransforms(1.7f,1.7f,1.7f,
                0,-90,0,
                0,0,-1.5f);
        chair.kd=1.7f;
        Model floor= makeModel(gl3, new ObjFromProperties("wall.properties"), "textured","floor.jpg");
        floor.setUpTransforms(1,1,1,
                0,0,0,
                0,0,0);
        Model backWall= makeModel(gl3, new ObjFromProperties("wall.properties"), "textured","wall.jpg");
        backWall.setUpTransforms(1,1,1,
                90,0,0,
                0,2,-4.5f);
        Model rightWall= makeModel(gl3, new ObjFromProperties("wall.properties"), "textured","brick-wall.jpg");
        rightWall.setUpTransforms(1,1,1,
                90,90,0,
                4.5f,2,0);
        Model leftWall= makeModel(gl3, new ObjFromProperties("wall.properties"), "textured","brick-wall.jpg");
        leftWall.setUpTransforms(1,1,1,
                -90,90,180,
                -4.5f,2,0);
        Model frontWall= makeModel(gl3, new ObjFromProperties("wall.properties"), "textured","wall-with-door.jpg");
        frontWall.setUpTransforms(1,1,1,
                90,0,0,
                0,2,4.5f);

        models.add(table);
        models.add(teapot);
        models.add(book);
        models.add(chair);
        models.add(floor);
        models.add(backWall);
        models.add(rightWall);
        models.add(leftWall);
        models.add(frontWall);

        lights.add(new LightSource());
        LightSource l2 = new LightSource();
        l2.lightPos=new float[]{-1f,4f,2f,1f};
//        l2.lightColor=new float[]{1.0f,1.0f,1.0f,1.0f};//gives bad color
        lights.add(l2);

        view.setUpCamera(
                0.0f, 5.0f, 7f,
                1.0f, 0.0f, -1.0f,
                0.0f, 1.0f, 0.0f
        );

        // Other GL initialization
        gl3.glEnable( GL.GL_DEPTH_TEST );
        gl3.glFrontFace( GL.GL_CCW );
        gl3.glPolygonMode( GL.GL_FRONT_AND_BACK, GL2GL3.GL_FILL );
        gl3.glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
        gl3.glDepthFunc( GL.GL_LEQUAL );
        gl3.glClearDepth( 1.0f );
        animator.start();
    }

    @Override
    public void display(GLAutoDrawable drawable) {

        // get the gl object
        GL3 gl3 = drawable.getGL().getGL3();

        // clear and draw params..
        gl3.glClear( GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT );

        Matrix projectionViewMatrix = view.getProjectionViewMatrix();

        for(Model model : models){
            drawModel(gl3,model,projectionViewMatrix);
        }
        animate();
    }

    private void animate() {

        //rotate the camera in a circle in the xz plane
        double radAngle = Math.toRadians(cameraAngle);
        double radius=6;
        float x= (float) (Math.cos(radAngle)*radius);
        float z= (float) (Math.sin(radAngle)*radius);
        view.eyePoint[0]=x;
        view.eyePoint[2]=z;
        view.lookAt[0]=-view.eyePoint[0];
//        view.lookAt[1]=-view.eyePoint[1];
        view.lookAt[2]=-view.eyePoint[2];

        cameraAngle+=0.5f;
    }

    private void drawModel(GL3 gl3,Model model,Matrix projectionViewMatrix){

        //use the shader associated with this model
        int shaderProgram = model.getShaderProgram();
        gl3.glUseProgram(shaderProgram);

        //make the composite matrix
        Matrix transformationMatrix = model.getTransformationMatrix();
//        Matrix compositeMatrix=projectionViewMatrix.times(transformationMatrix);
//        Matrix compositeMatrix=transformationMatrix.times(projectionViewMatrix);
        Matrix compositeMatrix=transformationMatrix;
//        compositeMatrix=view.getCompositeMatrix(transformationMatrix);

        //send this composite matrix down to the vertex shader
        int compositeMatrixLoc=gl3.glGetUniformLocation(shaderProgram, "compositeMatrix");
        float[] floats = toFloatArray(compositeMatrix);
        gl3.glUniformMatrix4fv(compositeMatrixLoc,1,false,floats,0);

        view.sendProjectionViewInfo(gl3,shaderProgram);


        model.sendDownLightInformation(gl3, Ia,lights);

        model.selectBuffers(gl3);
        gl3.glDrawElements( GL.GL_TRIANGLES, model.getNumberOfVertices(),
                GL.GL_UNSIGNED_SHORT, 0l
        );

    }

    private float[] toFloatArray(Matrix m){
        double[] doubleArray = m.getRowPackedCopy();

        int n=m.getRowDimension();
        float [] array=new float[n*n];
        for (int i = 0; i < doubleArray.length; i++) {
            array[i]=(float)doubleArray[i];
        }
        return array;
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {

    }

    private Texture loadTextureFromJogamp(String filename){
        String extension ="png";//default

        int i = filename.lastIndexOf('.');
        if (i > 0) {
            extension = filename.substring(i+1);
        }

        Texture texture=null;
        try {
            FileInputStream stream = new FileInputStream(filename);
            texture= TextureIO.newTexture(stream, false, extension);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Texture couldn't be loaded.Looks like the texture is not present in the classpath");
        }
        return texture;
    }

    /**
     * Create a vertex or element array buffer
     *
     * @param target - which type of buffer to create
     * @param data   - source of data for buffer (or null)
     * @param size   - desired length of buffer
     */
    int makeBuffer( int target, Buffer data, long size, GL3 gl3 )
    {
        int buffer[] = new int[1];

        gl3.glGenBuffers( 1, buffer, 0 );
        gl3.glBindBuffer( target, buffer[0] );
        gl3.glBufferData( target, size, data, GL.GL_STATIC_DRAW );

        return( buffer[0] );
    }


    private Model makeModel(GL3 gl3,ModelData modelData,String shaderFile){

        // get the vertices for the shape
        Buffer points = modelData.getVertices();
        long dataSize = modelData.nVertices() * 4l * 4l;

        // get the normals for the shape
        Buffer normals = modelData.getNormals();
        long ndataSize = modelData.nVertices() * 3l * 4l;

        // get the element data
        Buffer elements = modelData.getElements();
        long edataSize = modelData.nVertices() * 2l;

        long totalSize = dataSize + ndataSize ;

        int vertexBufferId = makeBuffer( GL.GL_ARRAY_BUFFER, null, totalSize, gl3 );
        gl3.glBufferSubData( GL.GL_ARRAY_BUFFER, 0, dataSize, points );
        gl3.glBufferSubData( GL.GL_ARRAY_BUFFER, dataSize, ndataSize, normals );

        // generate the element buffer
        int elementBufferId = makeBuffer( GL.GL_ELEMENT_ARRAY_BUFFER,
                elements, edataSize, gl3 );

        //make the shader program
        int shaderId=new ShaderProgram(gl3,shaderFile).getProgram();

        return new Model(shaderId,vertexBufferId,elementBufferId,modelData.nVertices());
    }

    private Model makeModel(GL3 gl3,ModelData modelData,String shaderFile,String textureFile){

        // get the vertices for the shape
        Buffer points = modelData.getVertices();
        long dataSize = modelData.nVertices() * 4l * 4l;

        // get the normals for the shape
        Buffer normals = modelData.getNormals();
        long ndataSize = modelData.nVertices() * 3l * 4l;

        // there may or may not be (u,v) information
        Buffer uv = modelData.getUV();
        long uvdataSize = modelData.nVertices() * 2l * 4l;


        // get the element data
        Buffer elements = modelData.getElements();
        long edataSize = modelData.nVertices() * 2l;

        long totalSize = dataSize + ndataSize + uvdataSize;

        int vertexBufferId = makeBuffer( GL.GL_ARRAY_BUFFER, null, totalSize, gl3 );
        gl3.glBufferSubData( GL.GL_ARRAY_BUFFER, 0, dataSize, points );
        gl3.glBufferSubData( GL.GL_ARRAY_BUFFER, dataSize, ndataSize, normals );
        gl3.glBufferSubData( GL.GL_ARRAY_BUFFER, dataSize+ndataSize,
                uvdataSize, uv );


        // generate the element buffer
        int elementBufferId = makeBuffer( GL.GL_ELEMENT_ARRAY_BUFFER,
                elements, edataSize, gl3 );

        //make the shader program
        int shaderId=new ShaderProgram(gl3,shaderFile).getProgram();

        //load the texture
        Texture texture=loadTextureFromJogamp(textureFile);

        return new Model(shaderId,vertexBufferId,elementBufferId,modelData.nVertices(),texture);
    }
}
