//Author: Nikhil Verma
import Jama.Matrix;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import javafx.scene.effect.Light;

import javax.media.opengl.GL;
import javax.media.opengl.GL3;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;

/**
 * Model that holds information about the transformation,buffer ids,and shader program.
 * Vertex data is not present in this class.
 * The per vertex names for position,normal and texture coordinates(if any)
 * are assumed to be "vPosition","vNormal" and "vTexCoord" respectively.
 * Created by NikhilVerma on 18/05/16.
 */
public class Model {

    private int shaderProgram;
    private int vertexBufferId;
    private int elementBufferId;
    private int numberOfVertices;

    private Texture texture=null;

    //transformation info
    private float x=0;
    private float y=0;
    private float z=0;
    private float rotationX=0;
    private float rotationY=0;
    private float rotationZ=0;
    private float scaleX=1;
    private float scaleY=1;
    private float scaleZ=1;

    public Model(int shaderProgram, int vertexBufferId, int elementBufferId, int numberOfVertices) {
        this(shaderProgram,vertexBufferId,elementBufferId,numberOfVertices,null);
    }

    public Model(int shaderProgram, int vertexBufferId, int elementBufferId, int numberOfVertices, Texture texture) {
        this.shaderProgram = shaderProgram;
        this.vertexBufferId = vertexBufferId;
        this.elementBufferId = elementBufferId;
        this.numberOfVertices = numberOfVertices;
        this.texture = texture;
    }

    private Texture loadFromJogamp(String filename){
        Texture texture=null;
        try {
            FileInputStream stream = new FileInputStream(filename);
            texture= TextureIO.newTexture(stream, false, "png");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Texture couldn't be loaded.Looks like the texture is not present in the classpath");
        }
        return texture;
    }
    //material properties
    float[] Oa ={0.5f,0.1f,0.9f,1.0f};
    float[] Od ={0.89f,0.0f,0.0f,1.0f};
//    private float[] Od ={0.89f,0.23f,0.89f,1.0f};
    float[] Os ={1.0f,1.0f,1.0f,1.0f};

    //reflective characteristics of the teapot
    float ka =0.5f;
    float kd =0.7f;
    float ks =1.0f;
    float ns =10.0f;

    public void setShaderProgram(int shaderProgram) {
        this.shaderProgram = shaderProgram;
    }

    public int getShaderProgram() {
        return shaderProgram;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public float getRotationX() {
        return rotationX;
    }

    public float getRotationY() {
        return rotationY;
    }

    public float getRotationZ() {
        return rotationZ;
    }

    public float getScaleX() {
        return scaleX;
    }

    public float getScaleY() {
        return scaleY;
    }

    public float getScaleZ() {
        return scaleZ;
    }

    public void setScale(float scaleX,float scaleY,float scaleZ){
        this.scaleX=scaleX;
        this.scaleY=scaleY;
        this.scaleZ=scaleZ;
        
    }

    public void setRotation(float rotationX,float rotationY,float rotationZ){
        this.rotationX=rotationX;
        this.rotationY=rotationY;
        this.rotationZ=rotationZ;
    }

    public void setTranslation(float x,float y,float z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    /**
     * This function sets up the transformation parameters for the vertices
     * of the shapes.
     * @param scaleX - amount of scaling along the x-axis
     * @param scaleY - amount of scaling along the y-axis
     * @param scaleZ - amount of scaling along the z-axis
     * @param rotateX - angle of rotation around the x-axis, in degrees
     * @param rotateY - angle of rotation around the y-axis, in degrees
     * @param rotateZ - angle of rotation around the z-axis, in degrees
     * @param translateX - amount of translation along the x axis
     * @param translateY - amount of translation along the y axis
     * @param translateZ - amount of translation along the z axis
     */
    public void setUpTransforms( float scaleX, float scaleY, float scaleZ,
                                 float rotateX, float rotateY, float rotateZ,
                                 float translateX, float translateY, float translateZ ){
        this.setScale(scaleX,scaleY,scaleZ);
        this.setRotation(rotateX,rotateY,rotateZ);
        this.setTranslation(translateX,translateY,translateZ);
    }

    public boolean isTextured() {
        return texture!=null;
    }

    public int getNumberOfVertices() {
        return numberOfVertices;
    }

    public int getElementBufferId() {
        return elementBufferId;
    }

    public int getVertexBufferId() {
        return vertexBufferId;
    }

    public Matrix getTransformationMatrix(){

        //scale
        Matrix scale=Matrix.identity(4,4);
        scale.set(0,0,scaleX);
        scale.set(1,1,scaleY);
        scale.set(2,2,scaleZ);

        //rotation along x
        double rotX=Math.toRadians(rotationX);
        Matrix rotationAlongX=Matrix.identity(4,4);
        rotationAlongX.set(1,1,Math.cos(rotX));
        rotationAlongX.set(1,2,Math.sin(rotX));
        rotationAlongX.set(2,1,-Math.sin(rotX));
        rotationAlongX.set(2,2,Math.cos(rotX));

        //rotation along y
        double rotY=Math.toRadians(rotationY);
        Matrix rotationAlongY=Matrix.identity(4,4);
        rotationAlongY.set(0,0,Math.cos(rotY));
        rotationAlongY.set(0,2,-Math.sin(rotY));
        rotationAlongY.set(2,0,Math.sin(rotY));
        rotationAlongY.set(2,2,Math.cos(rotY));

        //rotation along z
        double rotZ=Math.toRadians(rotationZ);
        Matrix rotationAlongZ=Matrix.identity(4,4);
        rotationAlongZ.set(0,0,Math.cos(rotZ));
        rotationAlongZ.set(0,1,Math.sin(rotZ));
        rotationAlongZ.set(1,0,-Math.sin(rotZ));
        rotationAlongZ.set(1,1,Math.cos(rotZ));

        Matrix rotation=rotationAlongX.times(rotationAlongY.times(rotationAlongZ));

        //translation
        Matrix translation=Matrix.identity(4,4);
        translation.set(3, 0, x);
        translation.set(3, 1, y);
        translation.set(3, 2, z);

//        return translation.times(rotation.times(scale));
        return scale.times(rotation.times(translation));
    }

    /**
     * Bind the correct vertex and element buffers
     *
     * Assumes the correct shader program has already been enabled
     */
    public void selectBuffers( GL3 gl3 )
    {
        // bind the buffers
        gl3.glBindBuffer( GL.GL_ARRAY_BUFFER, vertexBufferId );
        gl3.glBindBuffer( GL.GL_ELEMENT_ARRAY_BUFFER, elementBufferId );

        // calculate the number of bytes of vertex and normal data
        long dataSize = numberOfVertices* 4l * 4l;
        long ndataSize = numberOfVertices * 3l * 4l;

        // set up the vertex attribute variables
        int vPosition = gl3.glGetAttribLocation( shaderProgram, "vPosition" );
        gl3.glEnableVertexAttribArray( vPosition );
        gl3.glVertexAttribPointer( vPosition, 4, GL.GL_FLOAT, false,
                0, 0l );

        int vNormal = gl3.glGetAttribLocation( shaderProgram, "vNormal" );
        gl3.glEnableVertexAttribArray( vNormal );
        gl3.glVertexAttribPointer( vNormal, 3, GL.GL_FLOAT, false,
                0, dataSize );

        if( texture!=null ) {
            
            //hook up the texture coordinates
            int vTexCoord = gl3.glGetAttribLocation( shaderProgram, "vTexCoord" );
            gl3.glEnableVertexAttribArray( vTexCoord );
            gl3.glVertexAttribPointer( vTexCoord, 2, GL.GL_FLOAT, false,
                    0, dataSize+ndataSize );

            // send texture data down to the fragment shader
            sendDownTexture(gl3, "textureImage", 0);
        }
    }

    protected void sendDownTexture(GL3 gl3, String samplerName, int textureUnit) {
        gl3.glActiveTexture(GL.GL_TEXTURE0+textureUnit);
        texture.bind(gl3);
        texture.setTexParameteri(gl3,GL.GL_TEXTURE_MIN_FILTER,GL.GL_LINEAR);
        texture.setTexParameteri(gl3,GL.GL_TEXTURE_MAG_FILTER,GL.GL_LINEAR);
        int smileyTextureSampler=gl3.glGetUniformLocation(shaderProgram, samplerName);
        gl3.glUniform1i(smileyTextureSampler, textureUnit);
    }
    
    public void sendDownLightInformation(GL3 gl3,float [] Ia,Vector<LightSource> lightSources){
        //ambient material color
        sendDownVec4(shaderProgram,gl3,"Oa", Oa);
        //diffuse material color
        if(texture==null){
            sendDownVec4(shaderProgram,gl3,"Od", Od);
        }
        //specular material color
        sendDownVec4(shaderProgram,gl3,"Os", Os);

        //send reflective characteristics of the teapot

        //ambient reflection coefficient
        sendDownFloat(shaderProgram,gl3,"ka", ka);
        //diffuse reflection coefficient
        sendDownFloat(shaderProgram,gl3,"kd", kd);
        //specular reflection coefficient
        sendDownFloat(shaderProgram,gl3,"ks", ks);
        //specular exponent
        sendDownFloat(shaderProgram,gl3,"ns", ns);

        //ambient light in the scene
        sendDownVec4(shaderProgram,gl3,"Ia",Ia);


        //properties of the light sources

        final int size = lightSources.size();
        final int uniformLocation = gl3.glGetUniformLocation(shaderProgram, "totalLights");
        gl3.glUniform1i(uniformLocation,size);

        //send down all light information to shader as array
        float [] IdValues=new float[size *4];
        float [] IsValues=new float[size *4];
        float [] lightPositionValues=new float[size*4];

        for (LightSource lightSource: lightSources){
            int i=0;
            for (int j = 0; j < 4; j++,i++) {
                IdValues[j]=lightSource.lightColor[i];
                IsValues[j]=lightSource.lightColor[i];
                lightPositionValues[j]=lightSource.lightPos[i];
            }
        }

        //color
        sendDownVec4Array(shaderProgram, gl3, "Id", IdValues,size);
        //specular component of light
        sendDownVec4Array(shaderProgram, gl3, "Is", IsValues, size);
        //position
        sendDownVec4Array(shaderProgram,gl3,"lightPos",lightPositionValues,size);

    }

    private void sendDownVec4Array(int program, GL3 gl3, String attribute, float[] vec4, int totalElements){
        final int uniformLocation = gl3.glGetUniformLocation(program, attribute);
        gl3.glUniform4fv(uniformLocation,totalElements, vec4,0);
    }

    private void sendDownVec4(int program,GL3 gl3,String attribute,float [] vec4){
        final int uniformLocation = gl3.glGetUniformLocation(program, attribute);
        gl3.glUniform4fv(uniformLocation,1, vec4,0);
    }

    private void sendDownFloat(int program,GL3 gl3,String attribute,float value){
        final int uniformLocation = gl3.glGetUniformLocation(program, attribute);
        gl3.glUniform1f(uniformLocation,value);
    }
}
