//Author: Nikhil Verma
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Main program entry
 * Created by NikhilVerma on 18/05/16.
 */
public class finalMain{

    private static Frame frame;

    public static void main(String[] args) {

        Engine graphicsEngine=new Engine();

        frame = new Frame( "Nikhil Verma" );
        frame.setSize(600, 600);
        frame.add(graphicsEngine.getCanvas());
        frame.setVisible(true);

        // by default, an AWT Frame doesn't do anything when you click
        // the close button; this bit of code will terminate the program when
        // the window is asked to close
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                frame.dispose();
                System.exit(0);
            }
        });
    }
}
