//Author: Nikhil Verma
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Reads 3d information from a property file. The information is extracted from actual obj files.
 * Created by NikhilVerma on 20/05/16.
 */
public class ObjFromProperties extends ModelData{
    String propertyFile;

    public static final String VERTICES="vertices";
    public static final String NORMALS="normals";
    public static final String TEX_COORDINATES="textureCoordinates";
    public static final String VERTEX_ELEMENTS="elements";
    public static final String NORMAL_ELEMENTS="normalElements";
    public static final String TEXTURE_ELEMENTS="textureElements";
    private final Properties properties;

    public ObjFromProperties(String propertyFile) {
        this.propertyFile = propertyFile;
        properties = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(propertyFile);

            // load a properties file
            properties.load(input);
            make();

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * make the object from property file
     */
    private void make() {
        float[]vertices=readAttribute(VERTICES);
        float[] textureCoordinates=readAttribute(TEX_COORDINATES);
        float[] normals=readAttribute(NORMALS);
        int[] elements=readElements(VERTEX_ELEMENTS);
        int[] textureElements=readElements(TEXTURE_ELEMENTS);
        int[] normalElements=readElements(NORMAL_ELEMENTS);

        for( int i = 0; i < elements.length - 2; i += 3 ) {

            // calculate the base indices of the three vertices
            int point1 = 3 * (elements[i]-1); // slots 0, 1, 2
            int point2 = 3 * (elements[i + 1]-1); // slots 3, 4, 5
            int point3 = 3 * (elements[i + 2]-1); // slots 6, 7, 8

            int normal1 = 3 * (normalElements[i]-1);
            int normal2 = 3 * (normalElements[i + 1]-1);
            int normal3 = 3 * (normalElements[i + 2]-1);

            // Calculate the base indices of the three texture coordinates
            int tex1 = 2 * (textureElements[i]-1);     // slots 0, 1, 2
            int tex2 = 2 * (textureElements[i + 1]-1); // slots 3, 4, 5
            int tex3 = 2 * (textureElements[i + 2]-1); // slots 6, 7, 8

            addTriangleWithNormsAndUV(
                    (float) vertices[point1 + 0], (float) vertices[point1 + 1],
                    (float) vertices[point1 + 2],
                    (float) normals[normal1 + 0], (float) normals[normal1 + 1],
                    (float) normals[normal1 + 2],
                    (float) textureCoordinates[tex1 + 0], (float) textureCoordinates[tex1 + 1],

                    (float) vertices[point2 + 0], (float) vertices[point2 + 1],
                    (float) vertices[point2 + 2],
                    (float) normals[normal2 + 0], (float) normals[normal2 + 1],
                    (float) normals[normal2 + 2],
                    (float) textureCoordinates[tex2 + 0], (float) textureCoordinates[tex2 + 1],

                    (float) vertices[point3 + 0], (float) vertices[point3 + 1],
                    (float) vertices[point3 + 2],
                    (float) normals[normal3 + 0], (float)normals[normal3 + 1],
                    (float)normals[normal3 + 2],
                    (float) textureCoordinates[tex3 + 0], (float) textureCoordinates[tex3 + 1]
            );
        }
    }

    private float[] readAttribute(String key){

        String commaSepearted=properties.getProperty(key);
        String []values=commaSepearted.split(",");
        float attributes[]=new float[values.length];
        for (int i = 0; i < values.length; i++) {
            attributes[i]=Float.parseFloat(values[i]);
        }
        return attributes;
    }

    private int[] readElements(String key){

        String commaSeperated=properties.getProperty(key);
        String []values=commaSeperated.split(",");
        int elements[]=new int[values.length];
        for (int i = 0; i < values.length; i++) {
            elements[i]=Integer.parseInt(values[i]);
        }
        return elements;

    }
}
