//Author: Nikhil Verma
/**
 * Simple model holding the light information
 * Created by NikhilVerma on 18/05/16.
 */
public class LightSource {

    //properties of the light source
    float[] lightColor={1.0f,1.0f,0.0f,1.0f};
    float[] lightPos={0.0f,5.0f,2.0f,1.0f};

}
