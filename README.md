### Scene: Study Room

Author: **Nikhil Verma**

#### Brief
The Scene comprises of a room with four walls and a floor.There is a table and a chair in the center.A book and a
teapot also rest on the table. The camera circles around to give a full view. The preview below might seem a bit choppy. 
Thats from the screen recording software itself. The application itself runs at 60 fps. 

To run the program, compile and execute finalMain.java 

![Demo of the study room](http://res.cloudinary.com/madebynikhil/image/upload/v1473301861/study-room-demo_jcehoo.gif)  

#### Assets
The following assets were taken from the internet

Textures:
* wall.jpg
* wall-with-door.jpg
* wood.jpg
* book.bmp
* brick-wall.jpg

3D models:
* Table
* Chair
* Book
* Teapot(already provided)

Regarding the 3D models,I extracted the vertex information(tex coord,normals etc.) from raw obj files and manually
 placed them in my own formatted properties file which I read later (very time consuming,but I learned blender in
 the process).No third party obj reader was used.

I used some of the code from the previous assignments after refactoring them to my design. I used Jama to compute
the model-transformation matrix in the application code.

Shading was done on teapot which doesn't have any texture on it. Shading was also done on table and chair which both
have textures on them. The book ,the walls and the floor were textured but not phong shaded.

If you face any problems running the application, please let me know.

Nikhil Verma  
nxv9670@rit.edu